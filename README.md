# Kalkulačka #

Webová kalkulačka pro výpočet základních početních operací.

K vyzkoušení dostupná i online na http://calc.janhoffman.cz/

* Responzivní design
* Možnost ovládání klávesnicí se zvýrazněním stisknutého tlačítka
* Podporuje opakování poslední akce při opakovaném stiknutí "="
* Historie tří naposledy provedených operací

### Spuštění vývojového serveru ###

```
cd kalkulacka
yarn
yarn start
```

### Vytvoření produkční verze ###

```
cd kalkulacka
yarn run build
```

### Adresářová struktura ###

Vlastní zdrojový kód aplikace se nachází v `src/Calc/` a obsažených adresářích.
