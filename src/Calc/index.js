import React, { Component } from 'react'
import { adjust, merge, findLastIndex, path } from 'ramda'
import 'normalize.css'
import History from './History'
import Display from './Display'
import Button from './Button'
import './index.sass'

const numPadItems = [1, 2, 3, 4, 5, 6, 7, 8, 9]

class Calc extends Component {
  defaultState = {
    inputs: [],
    currentValue: '',
  }

  state = this.defaultState

  // Akce po stisknutí číselného tlačítka
  handleNumberClick = value => {
    this.setState(prevState => ({
      currentValue: `${prevState.currentValue}${value}`,
    }))
  }

  // Reset kalkulačky do výchozího stavu po stisknutí tlačítka AC
  handleReset = () => this.setState(this.defaultState)

  // Provede početní operaci dle zadaného operátoru
  doOperation = (operator, acc, value) => {
    if (typeof value !== 'number') {
      return acc
    }

    switch (operator) {
      case '+':
        return acc + value
      case '-':
        return acc - value
      case '×':
        return acc * value
      case '÷':
        return acc / value
      default:
        return value
    }
  }

  // Projde všechny zadané vstupy a spočte výsledek
  countResult = () => {
    let operator
    let calculations = []

    const value = this.state.inputs.reduce((acc, item, index) => {
      if (item.isOperator && item.value === '+/-') {
        calculations.push(`-(${acc}) = ${-acc}`)
        return -acc
      } else if (item.isOperator) {
        operator = item.value
        return acc
      }

      const operationResult = this.doOperation(operator, acc, item.value)

      if (operator === '=') {
        calculations = []
      } else {
        index > 0 && calculations.push(`${acc} ${operator} ${item.value} = ${operationResult}`)
      }

      return operationResult
    }, 0)

    return { value, calculations }
  }

  // Akce po stisknutí tlačítka s operátorem
  handleOperatorClick = operator => {
    this.setState(prevState => {
      // Ošetření pro inverzi znaménka
      if (operator === '+/-' && prevState.currentValue !== '') {
        return {
          currentValue: `-${prevState.currentValue}`,
        }
      }

      // Změna naposledy stisknutého operátoru
      if (operator === '=' && prevState.currentValue === '' && prevState.inputs.length) {
        const lastOperatorIndex = findLastIndex(
          item => item.isOperator && !['=', '+/-'].includes(item.value),
          prevState.inputs,
        )

        const numberIndex = typeof path(['inputs', lastOperatorIndex + 1, 'value'], prevState) === 'number'
          ? lastOperatorIndex + 1
          : lastOperatorIndex - 1

        if (lastOperatorIndex < 0 || numberIndex < 0) {
          return prevState
        }

        return {
          inputs: [
            ...prevState.inputs,
            { isOperator: true, value: path(['inputs', lastOperatorIndex, 'value'], prevState) },
            { isOperator: false, value: path(['inputs', numberIndex, 'value'], prevState) },
            { isOperator: true, value: '=' },
          ],
        }
      }

      // Ošetření pro opakovaný stisk "="
      if (prevState.inputs.length && prevState.currentValue === '' && operator !== '+/-') {
        return {
          inputs: adjust(
            item => merge(item, { value: operator }),
            prevState.inputs.length - 1,
            prevState.inputs,
          ),
        }
      }

      return {
        inputs: [
          ...prevState.inputs,
          ...prevState.currentValue !== '' ? [{ isOperator: false, value: parseFloat(prevState.currentValue) }] : [],
          { isOperator: true, value: operator },
        ],
        currentValue: '',
      }
    })
  }

  // Vrátí hodnotu pro displej s historií i hlavní displej
  getDisplayValue = () => {
    const { currentValue } = this.state
    const result = this.countResult()

    if (currentValue !== '') {
      return { value: currentValue, calculations: result.calculations }
    }

    return result
  }

  render() {
    const result = this.getDisplayValue()

    return (
      <div className="calc">
        <History items={result.calculations.slice(-3)} />
        <Display value={result.value} />

        <div className="calc__buttons">
          <div className="calc__numpad">
            <Button value="0" onClick={this.handleNumberClick} double />
            <Button value="." keycode="," onClick={this.handleNumberClick} />

            {numPadItems.map(number =>
              <Button key={number} value={number} onClick={this.handleNumberClick} />)}

            <Button value="AC" keycode="Delete" onClick={this.handleReset} double />
            <Button value="+/-" onClick={this.handleOperatorClick} />
          </div>

          <div className="calc__operators">
            <Button value="÷" keycode="/" onClick={this.handleOperatorClick} operator />
            <Button value="×" keycode="*" onClick={this.handleOperatorClick} operator />
            <Button value="-" onClick={this.handleOperatorClick} operator />
            <Button value="+" onClick={this.handleOperatorClick} operator />
            <Button value="=" keycode="Enter" onClick={this.handleOperatorClick} operator />
          </div>
        </div>
      </div>
    )
  }
}

export default Calc
