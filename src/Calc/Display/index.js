import React from 'react'
import Props from 'prop-types'
import './index.sass'

const Display = ({ value }) => (
  <div className="calc__display">
    {value}
  </div>
)

Display.propTypes = {
  value: Props.oneOfType([Props.string, Props.number]).isRequired,
}

export default Display
