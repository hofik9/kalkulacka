import React from 'react'
import Props from 'prop-types'
import { withHandlers, withState, lifecycle, compose } from 'recompose'
import './index.sass'

const Button = ({ value, double, operator, compKey, onClick }) => {
  const className = `calc__button ${double && 'calc__button--double'} ${operator && 'calc__button--operator'}`

  return (
    <button
      key={compKey}
      onClick={onClick}
      className={className}
    >
      {value}
    </button>
  )
}

Button.propTypes = {
  compKey: Props.number.isRequired,
  double: Props.bool,
  onClick: Props.func.isRequired,
  operator: Props.bool,
  value: Props.oneOfType([Props.string, Props.number]).isRequired,
}

Button.defaultProps = {
  double: false,
  operator: false,
}

export default compose(
  withState('compKey', 'setKey', 0),
  withHandlers({
    onClick: props => () => props.onClick(props.value),
  }),
  lifecycle({
    componentDidMount() {
      document.addEventListener('keyup', event => {
        if (event.key === (this.props.keycode || this.props.value.toString())) {
          this.props.setKey(Date.now())
          this.props.onClick(this.props.value)
        }
      })
    },
  })
)(Button)
