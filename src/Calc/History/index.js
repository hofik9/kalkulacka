import React from 'react'
import Props from 'prop-types'
import './index.sass'

const History = ({ items }) => (
  <div className="calc__history">
    <div className="calc__history-content">
      {items.map((item, index) => <span key={index} className="calc__history-item">{item}</span>)}
    </div>
  </div>
)

History.propTypes = {
  items: Props.arrayOf(Props.string).isRequired,
}

export default History
