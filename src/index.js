import React from 'react'
import ReactDOM from 'react-dom'
import Calc from './Calc'
import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(<Calc />, document.getElementById('root'))
registerServiceWorker()
