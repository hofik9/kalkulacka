module.exports = {
  extends: [
    '@strv/javascript/environments/react/v15',
    '@strv/javascript/environments/react/optional',
    '@strv/javascript/coding-styles/recommended',
    '@strv/javascript/coding-styles/react',
  ],
  env: {
    browser: true,
    node: true,
    jest: true,
    es6: true,
  },
  parserOptions: {
    sourceType: 'module',
  },
  parser: 'babel-eslint',
  "settings": {
    "import/resolver": {
      "babel-module": {}
    }
  },
  rules: {
    'import/exports-last': 0,
    'operator-linebreak': 0,
    'no-implicit-coercion': 0,
    'no-warning-comments': 0,
    'no-use-before-define': 'error',
    'no-alert': 2,
    'comma-dangle': ['error', 'always-multiline'],
  }
}
